<?php
// A simple web site in Cloud9 that runs through Apache
// Press the 'Run' button on the top to start the web server,
// then click the URL that is emitted to the Output tab of the console

$query = htmlspecialchars($_GET["query"]);
$url = "https://queryfeed.net/twitter?q=" . $query;
$xml = simplexml_load_string(file_get_contents($url), 'SimpleXMLElement', LIBXML_NOCDATA);

$json = json_encode($xml);
echo $json;
?>