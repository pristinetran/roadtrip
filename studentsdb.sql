-- CLI commands
-- first command to open the mysql-cli: mysql-ctl cli
-- second command to execute this entire SQL file: mysql -hlocalhost -uroot students < studentsdb.sql

DROP TABLE IF EXISTS lession_table;
DROP TABLE IF EXISTS class_table;
DROP TABLE IF EXISTS student_table;
DROP DATABASE IF EXISTS students;
CREATE DATABASE students;
USE students;

CREATE TABLE IF NOT EXISTS `student_table` (
  `studentId` int(10) unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `firstName` varchar(25) NOT NULL,
  `lastName` varchar(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `class_table` (
  `id` int(10) unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `className` varchar(25) NOT NULL,
  `period` varchar(25) NOT NULL,
  `studentId` varchar(25) NOT NULL,
  `classId` varchar(25) NOT NULL,
  CONSTRAINT FK_StudentClass FOREIGN KEY (studentId)
   REFERENCES student_table(studentId)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `lession_table` (
  `id` int(10) unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `lessionName` varchar(25) NOT NULL,
  `classId` int(12) unsigned NOT NULL,
  CONSTRAINT FK_ClassAssignment FOREIGN KEY (classId)
  REFERENCES class_table(classId)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- review number of tables;
SHOW TABLES;

-- insert students
INSERT INTO student_table (firstName, lastName) 
    VALUES ('Kevin', 'Parker'),
           ('Steven', 'Tray'),
           ('Brittany', 'Chase'),
           ('Jessica', 'Smith');

-- insert classes
INSERT INTO class_table (className, period, studentId, classId) 
    VALUES  ('English', 'Period 4', 1, 2), 
            ('Science', 'Period 5', 1, 3),
            ('Math', 'Period 2',    2, 1),
            ('Science', 'Period 5', 2, 3),
            ('Math', 'Period 2',    3, 1),
            ('English', 'Period 4', 3, 2),
            ('Science', 'Period 5', 3, 3),
            ('Math', 'Period 2',    4, 1),
            ('English', 'Period 4', 4, 2);
            
-- review schema
DESCRIBE lession_table;

INSERT INTO lession_table (lessionName, classId) 
    VALUES  ('Intro', 1),
            ('Geometry', 1),
            ('Algebra', 1),
            ('Intro', 2),
            ('British Literature', 2),
            ('Intro', 3),
            ('Algebra', 3),
            ('Physics', 3);

-- show all of the lessions User 1 is taking
SELECT lt.lessionName AS LESSIONS
FROM student_table AS st
LEFT JOIN class_table AS ct ON st.studentId = ct.studentId
LEFT JOIN lession_table AS lt ON lt.classId = ct.classId
WHERE st.studentId =1;

SELECT CONCAT_WS(" ", st.firstName, st.lastName) AS FULLNAME_OF_ATTENDEES
FROM student_table AS st
LEFT JOIN class_table AS ct ON st.studentId = ct.studentId
WHERE ct.period = "Period 5";